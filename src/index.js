import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import * as serviceWorker from './serviceWorker';
import Login from './components/App/Login';
import DashboardSeller from './components/App/DashboardSeller';
import DashboardClient from './components/App/DashboardClient';
import Register from './components/App/Register';
import ButtonAppBar from './components/ButtonAppBar'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import pink from '@material-ui/core/colors/pink';
import purple from '@material-ui/core/colors/purple';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import {urls} from "./util/urlUtils";

const theme = createMuiTheme({
  palette: {
    primary: pink,
    secondary: {
      main: '#f44336',
    },
    best: purple
  },
});

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <Router>
          <div>
            <ButtonAppBar/>
            <Route exact path={urls.home.path} component={App} />
            <Route path={urls.login.path} component={Login} />
            <Route path={urls.register.path} component={Register} />
            <Route path={urls.dashboardClient.path} component={DashboardClient} />
            <Route path={urls.dashboardSeller.path} component={DashboardSeller} />
          </div>
      </Router>
    </MuiThemeProvider>,
    document.getElementById('root')
  )

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
