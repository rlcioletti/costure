import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem'
import { Link, browserHistory,withRouter } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import { firebaseAuth } from '../util/firebaseUtils';
import logo from '../youmodlogo.png';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class ButtonAppBar extends React.Component {
  state = {
    anchorEl: null,
    left: false
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  profileClick = () => {
    this.props.history.push('/DashboardClient');
  }

  addProductClick = () => {
    this.props.history.push('/DashboardClient');
  } 

  logout = () => {
    firebaseAuth.signOut();
    this.handleClose();
    this.props.history.push('/');
  }

  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
            <Link style={{textDecoration: 'none', color: '#ffffff'}} to='/'>
              U-Dress
            </Link>
            </Typography>
              {firebaseAuth.currentUser ? (
                <div>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                <MenuItem onClick={this.profileClick}>Perfil</MenuItem>
                <MenuItem onClick={this.addProductClick}>Adicionar Produto</MenuItem>
                <MenuItem onClick={this.logout}>Sair</MenuItem>
                </Menu>
                </div>
              ) : (
                <Link to='/Login' style={{textDecoration: 'none'}}>
                  <Button color='primary' variant="contained">Login</Button>
                </Link>
              )}  
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(ButtonAppBar));