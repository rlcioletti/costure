import React, { Component } from 'react';
import './App.css';
import Item from '../Item';
import Grid from '@material-ui/core/Grid';
import FirebaseService from '../../services/FirebaseService'

const container = {
  padding: 30
  };
 
class App extends Component {

  state = {
    data: []
  };

  componentDidMount() {
      FirebaseService.getDataList('products', (dataReceived) => this.setState({data: dataReceived}));
  }

  render() {
    return (
        <div style={container}>
        <Grid container spacing={40} direction="row" justify="flex-start" alignItems="flex-start">
          {
            this.state.data.map((e, index) => {
              return(
                <Item name={e.name} url={e.url_photo} description={e.description} rating={e.rating}/>
              );
            })
          }
        </Grid>  
        </div>
    );
  }
}

export default App;
