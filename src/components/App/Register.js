import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';
import { firebaseAuth } from '../../util/firebaseUtils';

class Register extends Component {

    constructor(){
      super();
      this.checkIsLogged = this.checkIsLogged.bind(this);

      this.state = {
        user: undefined,
        email: '',
        password: '',
      }

      this.checkIsLogged();
    }
  
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };

    register() {
      firebaseAuth.createUserWithEmailAndPassword(this.state.email , this.state.password)
      .then((user) => {
        console.log(user);
        this.checkIsLogged();
      })
      .catch(function(error) {
        console.error(error);
      });
    }

    checkIsLogged = () => {
      console.log('checando se esta logado');
      firebaseAuth.onAuthStateChanged((user) => {
        if (user) {
          console.log('logado');
          this.props.history.push('/DashboardClient')
        } else {
          console.log('não logado');
        }
      });
    }

    render() {
      return (
        <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', padding: 45}} >
        <Grid style={{borderColor: '#d6d7da', height: 200, width: 500, padding: 50, justifyContent:'center', alignItems:'center', textAlign: 'center'}}>
        <form onSubmit={this.register}>
        <TextField
              id="name"
              label="Nome"
              type="name"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={this.handleChange('name')}
          />
          <TextField
              id="email"
              label="Email"
              type="email"
              autoComplete="email"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={this.handleChange('email')}
          />
          <TextField
              id="password"
              label="Senha"
              type="password"
              margin="normal"
              variant="outlined"
              fullWidth
              ref="password"
              onChange={this.handleChange('password')}
          />
        <div>  
        <Switch
          onChange={this.handleChange('checkedA')}
          value="checkedA" />
        <p>Você deseja se cadastrar como vendendor ?</p>
        </div>
        <Button type='submit' color='primary' variant="contained" fullWidth>Criar Conta</Button><br/>
        </form>
        </Grid>
        </div>
      );
    }
  }

  export default Register;