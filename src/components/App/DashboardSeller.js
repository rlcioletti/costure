import React, { Component } from 'react';
import { Grid, Button, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import DraftsIcon from '@material-ui/icons/Drafts';
import InboxIcon from '@material-ui/icons/Inbox';

class DashboardSeller extends Component {
    render() {

      function ListItemLink(props) {
        return <ListItem button component="a" {...props} />;
      }

      return (
        <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', height: '100vh'}}>
        <Grid>
        <List component="nav">
        <ListItem button>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Novas Mensagens" />
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <DraftsIcon />
          </ListItemIcon>
          <ListItemText primary="Mensagens antigas" />
        </ListItem>
      </List>
        </Grid>
          <Grid container style={{borderColor: '#d6d7da', height: 500, width: 500, background: '#7b7b7b55'}}>
            <Grid container alignItems='flex-end' alignContent='flex-end' justify='flex-end'>
            <text>Mensagem de teste Chat</text>
            <Button fullWidth color='primary' variant='contained' style={{justifyContent: 'end'}}>Enviar</Button>
            </Grid>
          </Grid>
        </div>
      );
    }
  } 

export default DashboardSeller;