// modules/Repos.js
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { firebaseAuth } from '../../util/firebaseUtils';
import { Link } from 'react-router-dom'; 

class Login extends Component {

    email = 'rlcioletti@gmail.com';
    password = '123456';

    constructor() {
      super();
      this.authenticate = this.authenticate.bind(this);
      this.checkIsLogged = this.checkIsLogged.bind(this);
      
      this.state = {
        email: '',
        password: '',
      }

      this.checkIsLogged();
    }

    componentDidMount(){
      console.log('Usuário logado: ' + firebaseAuth.currentUser);
      console.log(firebaseAuth.currentUser)
    }

    authenticate(e) {
      e.preventDefault();
      console.log('authenticating');

      firebaseAuth.signInWithEmailAndPassword(this.state.email, this.state.password).then(signedUser => {
        console.log(signedUser);
        this.checkIsLogged();
      }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorCode, ' - ', errorMessage);
      });
    }

    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };

    checkIsLogged = () => {
      console.log('checando se esta logado');
      firebaseAuth.onAuthStateChanged((user) => {
        if (user) {
          console.log('logado');
          this.props.history.push('/DashboardClient')
        } else {
          console.log('não logado');
        }
      });
    }

    render() {
      return (
        <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', padding: 45}} >
        <Grid style={{borderColor: '#d6d7da', height: 200, width: 500, padding: 50, justifyContent:'center', alignItems:'center', textAlign: 'center'}}>
        <form onSubmit={this.authenticate}>
          <TextField
              id="email"
              label="Login"
              type="email"
              autoComplete="email"
              margin="normal"
              variant="outlined"
              fullWidth
              ref="email"
              onChange={this.handleChange('email')}
          />
          <TextField
              id="password"
              label="Senha"
              type="password"
              margin="normal"
              variant="outlined"
              fullWidth
              ref="password"
              onChange={this.handleChange('password')}
          />
        <Button type='submit' color='primary' variant="contained" fullWidth>Login</Button><br/>
        </form>
        <h5>Nao tem conta ainda ? <p><Link to='/Register' style={{color: '#ff000070', textDecoration: 'none'}}>Cadastre-se</Link></p></h5>
        </Grid>
        </div>
      );
    }
  }

export default Login;