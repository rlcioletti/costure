import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { List, ListItemIcon, Divider, ListItemText, ListItem } from '@material-ui/core'
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import FirebaseService from '../../services/FirebaseService'
import Avatar from '@material-ui/core/Avatar';
import {urls} from "../../util/urlUtils";
import {withRouter} from "react-router-dom";

class DashboardClient extends Component {
    state = {
      selectedIndex: 0,
      mensagem: '',
    };

    handleListItemClick = (event, index) => {
      this.setState({ selectedIndex: index });
    };

    renderSection() {
      if(this.state.selectedIndex === 0){
        return <MyProducts/>;
      }else if(this.state.selectedIndex === 1){
        return <Chats/>;
      }else if(this.state.selectedIndex === 2){
        return <Config/>;
      }else if(this.state.selectedIndex === 3){
        return <AddProducts/>;
      }
    }

    render() {
      return (
        <Grid container alignItems="stretch">
          <Grid item xs={2}>
          <List component="nav">
            <ListItem
              button
              selected={this.state.selectedIndex === 0}
              onClick={event => this.handleListItemClick(event, 0)}
            >
              <ListItemIcon>
              <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Meus Produtos" />
            </ListItem>
            <ListItem
              button
              selected={this.state.selectedIndex === 1}
              onClick={event => this.handleListItemClick(event, 1)}
            >
              <ListItemIcon>
              <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="Chats" />
            </ListItem>
          </List>
          <Divider />
          <List component="nav">
            <ListItem
              button
              selected={this.state.selectedIndex === 2}
              onClick={event => this.handleListItemClick(event, 2)}
            >
              <ListItemText primary="Configurações" />
            </ListItem>
            <ListItem
              button
              selected={this.state.selectedIndex === 3}
              onClick={event => this.handleListItemClick(event, 3)}
            >
              <ListItemText primary="Cadastrar Produto" />
            </ListItem>
          </List>
          </Grid>
          {this.renderSection()}
        </Grid>
      );
    }
  }

  class MyProducts extends Component {
    render() {
      return (
        <Grid xs={10} style={{ justifyContent:'center', alignItems:'center', textAlign: 'center'}}>
              <h1>Meus Produtos</h1>
              <span>Voçê ainda não possui nenhum produto.</span>
        </Grid>
      );
    }
  }

  class Chats extends Component {
    render() {
      return (
        <Grid xs={10} container justify="space-evenly" alignItems="center" direction="column">
        <Title/>     
        <MessageList/>      
        <SendMessageForm/>      
        </Grid>
      );
    }
  }

  class Title extends Component {
    render(){
      return (
      <Grid>
        <h1>Chats</h1>
        <h4>Pedro dos anjos - Calça de Algodão</h4>
      </Grid>
      );
    }
  }

  class MessageList extends Component {
    render(){
      return (
      <Grid justify="center"
      alignItems="center">
        <Paper style={{height: 400, width: 700}}>
                <p>Boa tarde Pedro</p>
        </Paper>
      </Grid>
      );
    }
  }

  class SendMessageForm extends Component {
    render(){
      return (
      <Grid>
        <TextField
          id="standard-bare"
          placeholder="Write text to send"
          margin="normal"
          style={{width: 600}} />
        <Button color='primary' variant="contained" xs={4}>Enviar</Button>
      </Grid>
      );
    }
  }

  class Config extends Component {

    render() {
      return (
        <Grid xs={10} container justify="space-evenly" alignItems="center" direction="column">
        <h1>Perfil</h1>
        <form onSubmit={this.submit}>
        <input
        accept="image/*"
        id="outlined-button-file"
        multiple
        type="file"
        style={{display: 'none'}}
      />
      <label >
      <Avatar alt="Rodrigo Cioletti" src="https://avatars0.githubusercontent.com/u/18273867?s=460&v=4" component='span' style={{margin: 10, width: 60, height: 60}} />
      </label>
          <TextField
              id="nome"
              label="Nome"
              type="nome"
              autoComplete="nome"
              margin="normal"
              variant="outlined"
              fullWidth
          />
          <TextField
              id="desc"
              label="Descrição"
              type="desc"
              margin="normal"
              variant="outlined"
              fullWidth
          />
          <TextField
              id="link"
              label="Link da Imagem"
              type="link"
              margin="normal"
              variant="outlined"
              fullWidth
              ref="link"
          />
          <TextField
              id="adress"
              label="Endereço"
              type="adress"
              margin="normal"
              variant="outlined"
              fullWidth
              ref="adress"
          />
        <Button type='submit' color='primary' variant="contained" fullWidth>Salvar Configurações</Button><br/>
        </form>
        </Grid>
      );
    }
  }

  class AddProducts extends Component {

    getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min)) + min;
    }

    submit = (event) => {
      event.preventDefault();

      const {name} = this;
      const {description} = this;
      const {url_photo} = this;
      const rating = this.getRandomInt(1,5);

      const newid = FirebaseService.pushData('products', {
        name,
        description,
        url_photo,
        rating
      });

      this.props.history.push(urls.home.path);
    }

    render() {
      return (
        <Grid xs={10} container justify="space-evenly" alignItems="center" direction="column">
        <h1>Cadastrar Produto</h1>
        <form onSubmit={this.submit}>
          <TextField
              id="nome"
              label="Nome"
              type="nome"
              autoComplete="nome"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={e => this.name = e.target.value}
          />
          <TextField
              id="desc"
              label="Descrição"
              type="desc"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={e => this.description = e.target.value}
          />
          <TextField
              id="link"
              label="Link da Imagem"
              type="link"
              margin="normal"
              variant="outlined"
              fullWidth
              onChange={e => this.url_photo = e.target.value}
          />
        <Button type='submit' color='primary' variant="contained" fullWidth>Cadastrar Produto</Button><br/>
        </form>
        </Grid>
      );
    }
  }

  

  export default DashboardClient;