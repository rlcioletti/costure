import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Icon from '@material-ui/core/Icon';

  const container = {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  }
  
class Item extends Component {

  ratingBar = (rating) => {

    var stars = [];

    for(var i = 0; i < 5; i++){
      if (rating > i){
        console.log(rating)
        stars.push(<Icon style={{color: '#FFB300'}}>star</Icon>);
      } else {
        stars.push(<Icon style={{color: '#FFB300'}}>star_border</Icon>);
      }
    }
    return stars;
  }

  render() {
    return (
      <Grid item xs={3} style={container} justify="space-evenly">
      <Paper>
        <img style={{width: 200, height: 300}} src={this.props.url} alt="random" />
        <h4>{this.props.name}</h4>
        <p style={{fontSize: 13}}>{this.props.description}</p>
        {this.ratingBar(this.props.rating)}
        <Button variant="contained" color="primary" fullWidth>
          Ver Detalhes
        </Button>
      </Paper>
      </Grid>
    );
  }
  }

export default Item;