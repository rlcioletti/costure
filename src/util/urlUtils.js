export const urls = {
    home: { name: 'Home', path:'/'},
    login: { name: 'Login', path:'/Login'},
    register:  { name: 'register',  path:'/Register'},
    dashboardClient:  { name: 'DashboardClient',  path:'/DashboardClient'},
    dashboardSeller:  { name: 'DashboardSeller',  path:'/DashboardSeller'},
};