import firebase from 'firebase';

var config = {
    apiKey: "AIzaSyB5LiSrd7SjOCRibV4IVfZHz0DgimcG-rs",
    authDomain: "costure-e265f.firebaseapp.com",
    databaseURL: "https://costure-e265f.firebaseio.com",
    projectId: "costure-e265f",
    storageBucket: "costure-e265f.appspot.com",
    messagingSenderId: "603891074625"
};

export const firebaseImpl = firebase.initializeApp(config);
export const firebaseDatabase = firebase.database();
export const firebaseAuth = firebase.auth();